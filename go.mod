module wangchunguang_blog

go 1.12

require (
	github.com/alimoeeny/gooauth2 v0.0.0-20140214171402-62c620a8c7eb
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	github.com/claudiu/gocron v0.0.0-20151103142354-980c96bf412b
	github.com/dchest/captcha v0.0.0-20170622155422-6a29415a8364
	github.com/denisbakhtin/sitemap v0.0.0-20151103020935-3b73dfe0369c
	github.com/gin-contrib/sessions v0.0.1
	github.com/gin-gonic/gin v1.4.0
	github.com/gorilla/feeds v1.1.1
	github.com/jinzhu/gorm v1.9.11
	github.com/kr/pretty v0.1.0 // indirect
	github.com/microcosm-cc/bluemonday v1.0.2
	github.com/qiniu/api.v7 v7.2.5+incompatible
	github.com/qiniu/x v7.0.8+incompatible // indirect
	github.com/russross/blackfriday v2.0.0+incompatible
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/snluu/uuid v0.0.0-20130306162636-1dd34a9ad6c0
	gopkg.in/yaml.v2 v2.2.4
	qiniupkg.com/x v7.0.8+incompatible
)
