package model

import (
	"github.com/jinzhu/gorm"
	"time"
)

// 用户订阅 数据表
type Subscriber struct {
	gorm.Model
	Email          string    `gorm:"unique_index default:null comment '邮箱'"` //邮箱
	VerifyState    bool      `gorm:"default:'0' comment '验证状态'"`             //验证状态
	SubscribeState bool      `gorm:"default:'1' comment '订阅状态'"`             //订阅状态
	OutTime        time.Time `gorm:"default:null comment '过期时间'"`            //过期时间
	SecretKey      string    `gorm:"default:null comment '秘钥'"`              // 秘钥
	Signature      string    `gorm:"default:null comment '签名'"`              //签名
}
