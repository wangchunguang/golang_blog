package model

import "time"

// 数据结果
type QrArchive struct {
	ArchiveDate time.Time //月份
	Total       int       //总数
	Year        int       // 年份
	Month       int       // 月份

}
