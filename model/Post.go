package model

//  帖子 数据表
type Post struct {
	BaseModel
	Title        string     `gorm:"default:null comment '标题'"`           // 标题
	Body         string     `gorm:"type:text default:null comment '主体'"` // 主体
	View         int        `gorm:"default:0 comment '浏览次数'"`            // 浏览次数
	IsPublished  bool       `gorm:"default:0 comment '是否发表'"`            // 是否发表
	Tags         []*Tag     `gorm:"-"`                                   // 标签的数量
	Comments     []*Comment `gorm:"-"`                                   // 评论的信息
	CommentTotal int        `gorm:"-"`                                   // 评论的数量
}
