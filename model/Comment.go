package model

//  评论 数据表
type Comment struct {
	BaseModel
	UserID    uint   // 用户id
	Content   string `gorm:"default:null comment '内容'"`                    // 内容
	PostID    uint   `gorm:"column:post_id; default:null comment '文章id'"`  // 文章id
	ReadState bool   `gorm:"column:read_state;default:'0' comment '阅读状态'"` // 阅读状态
	NickName  string `gorm:"-"`                                            // "_" 忽略本字段
	AvatarUrl string `gorm:"-"`
	GithubUrl string `gorm:"-"`
}
