package model

// 标签 数据表
type Tag struct {
	BaseModel
	Name  string `gorm:"default:null comment '标签名称'"` // 标签名称
	Total int    `gorm:"-"`                           // 计数
}
