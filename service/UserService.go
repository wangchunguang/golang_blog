package service

import (
	"github.com/jinzhu/gorm"
	"wangchunguang_blog/model"
)

// 获取user
func GetUser(id interface{}) (*model.User, error) {
	var user model.User
	err := db.First(&user, id).Error
	return &user, err
}

// 新增user
func UserInsert(user *model.User) error {
	return db.Save(user).Error
}

// 通过用户名获取用户
func GetUserByUsername(useranme string) (*model.User, error) {
	var user model.User
	e := db.First(&user, "email =?", useranme).Error
	return &user, e
}

// 通过github唯一标识获取用户
func GetIsGithubIdExists(githubId string, id uint) (*model.User, error) {
	var user model.User
	err := db.First(&user, "github_login_id = ? and id != ?", githubId, id).Error
	return &user, err
}

// 修改用户信息
func UpdateGithubUserInfo(user *model.User) error {
	var githubLoginId interface{}
	if len(user.GithubLoginId) == 0 { // 唯一表示为空 设置为nil
		githubLoginId = gorm.Expr("NULL")
	} else {
		githubLoginId = user.GithubLoginId
	}
	err := db.Model(user).Updates(map[string]interface{}{
		"github_login_id": githubLoginId,
		"avatar_url":      user.AvatarUrl,
		"github_url":      user.GithubUrl,
	}).Error
	return err
}

// 通过githubloginid 获取用户
func GetFirstOrCreate(user *model.User) (*model.User, error) {
	//	 获取最新一条记录或者创建一条新记录
	err := db.FirstOrCreate(&user, "github_login_id = ?", user.GithubLoginId).Error
	return user, err
}

// 获取所有用户
func ListUsers() ([]*model.User, error) {
	var user []*model.User
	err := db.Find(&user, "is_admin =?", false).Error
	return user, err
}

// 获取用户的锁
func Lock(user *model.User) error {
	return db.Model(user).Updates(map[string]interface{}{
		"lock_state": user.LockState,
	}).Error
}

// 修改昵称
func UpdateProfile(user *model.User, avatarUrl, nickName string) error {
	return db.Model(&user).Update(model.User{AvatarUrl: avatarUrl, NickName: nickName}).Error
}

// 修改邮箱
func UpdateUserEmail(user *model.User, email string) error {
	if len(email) > 0 {
		return db.Model(user).Update("email", email).Error
	} else {
		return db.Model(user).Update("emaol", gorm.Expr("NULL")).Error
	}

}
