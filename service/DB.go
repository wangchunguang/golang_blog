package service

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"log"
	"time"
	"wangchunguang_blog/system"
)

var db *gorm.DB
var err error

// 连接数据库
func ConnectDB() {

	db, err = gorm.Open("mysql", system.GetConfiguration().DSN)
	if err != nil {
		log.Panicln("连接数据库失败: " + err.Error())
	}
	// 最大空闲数
	db.DB().SetMaxIdleConns(10)
	//	最大连接数
	db.DB().SetMaxOpenConns(50)
	// 最长连接时间
	db.DB().SetConnMaxLifetime(5 * time.Minute)
	//	 开启日志
	db.LogMode(true)
	// 创建数据表的时候 当新增数据表或第一次运行需要打开注释
	//CreateTable()
}

// 从数据库断开
func DisconnectDB() {
	if err := db.Close(); nil != err {
		log.Println("数据库断开失败：" + err.Error())
	}
}
