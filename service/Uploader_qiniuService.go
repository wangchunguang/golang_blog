package service

import (
	"context"
	"fmt"
	"github.com/qiniu/api.v7/auth/qbox"
	"github.com/qiniu/api.v7/storage"
	"mime/multipart"
	"os"
	"wangchunguang_blog/system"
)

// 获取文件大小接口
type Size interface {
	Size() int64
}

// 获取文件信息接口
type Stat interface {
	// 文件解析状态
	Stat() (os.FileInfo, error)
}

// 构造返回字段
type PutRet struct {
	Hash string `json:"hash"`
	Key  string `json:"key"`
}

type QiniuUploader struct {
}

// 加载
func (u QiniuUploader) Upload(file multipart.File, fileHeader *multipart.FileHeader) (url string, err error) {
	var (
		ret  PutRet
		size int64
	)
	if statInterface, ok := file.(Stat); ok {
		// 获取文件状态
		fileInfo, _ := statInterface.Stat()
		size = fileInfo.Size()
	}
	if sizeInterface, ok := file.(Size); ok {
		size = sizeInterface.Size()
	}
	putPolicy := storage.PutPolicy{
		Scope: system.GetConfiguration().BackupKey,
	}
	//NewMac 构建一个新的拥有AK/SK的对象
	mac := qbox.NewMac(system.GetConfiguration().QiniuAccessKey, system.GetConfiguration().QiniuSecretKey)
	//UploadToken 方法用来进行上传凭证的生成
	token := putPolicy.UploadToken(mac)
	cfg := storage.Config{}
	// 上传的机房
	cfg.Zone = &storage.ZoneHuanan
	// 不启用https域名
	cfg.UseHTTPS = false
	// 不适用cnd加速
	cfg.UseCdnDomains = false
	// NewFormUploader 用来构建一个表单上传的对象
	uploader := storage.NewFormUploader(&cfg)
	//PutExtra 为表单上传的额外可选项
	putExtra := storage.PutExtra{}
	//	 上传文件

	err = uploader.PutWithoutKey(context.Background(), &ret, token, file, size, &putExtra)
	if err != nil {
		fmt.Println(err)
		return
	}
	url = system.GetConfiguration().QiniuFileServer + ret.Key
	return

}
