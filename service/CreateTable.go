package service

import (
	"wangchunguang_blog/model"
)

func createTable() {
	// 创建表
	// 判断表是否存在 db.HasTable
	// 设置表的属性 db.set
	//创建评论数据表
	if !db.HasTable(&model.Comment{}) {
		if err := db.Set("gorm:table_options", "ENGINE=InnoDB DEFAULT CHARSET=utf8 comment '评论数据表'").CreateTable(&model.Comment{}).Error; err != nil {
			panic(err)
		}
	}
	// 创建用户订阅表
	if !db.HasTable(&model.Link{}) {
		if err := db.Set("gorm:table_options", "ENGINE=InnoDB DEFAULT CHARSET=utf8 comment '用户订阅数据表'").CreateTable(&model.Link{}).Error; err != nil {
			panic(err)
		}
	}
	// 分页 数据表
	if !db.HasTable(&model.Page{}) {
		if err := db.Set("gorm:table_options", "ENGINE=InnoDB DEFAULT CHARSET=utf8 comment '分页'").CreateTable(&model.Page{}).Error; err != nil {
			panic(err)
		}
	}
	//  帖子 数据表
	if !db.HasTable(&model.Post{}) {
		if err := db.Set("gorm:table_options", "ENGINE=InnoDB DEFAULT CHARSET=utf8 comment '帖子'").CreateTable(&model.Post{}).Error; err != nil {
			panic(err)
		}
	}
	//  标签 帖子 中间表
	if !db.HasTable(&model.PostTag{}) {
		if err := db.Set("gorm:table_options", "ENGINE=InnoDB DEFAULT CHARSET=utf8 comment ' 标签 帖子 中间表'").CreateTable(&model.PostTag{}).Error; err != nil {
			panic(err)
		}
	}
	//  用户订阅 数据表
	if !db.HasTable(&model.Subscriber{}) {
		if err := db.Set("gorm:table_options", "ENGINE=InnoDB DEFAULT CHARSET=utf8 comment '用户订阅 数据表'").CreateTable(&model.Subscriber{}).Error; err != nil {
			panic(err)
		}
	}
	// 标签 数据表
	if !db.HasTable(&model.Tag{}) {
		if err := db.Set("gorm:table_options", "ENGINE=InnoDB DEFAULT CHARSET=utf8 comment '标签 数据表'").CreateTable(&model.Tag{}).Error; err != nil {
			panic(err)
		}
	}
	// 用户 数据表
	if !db.HasTable(&model.User{}) {
		if err := db.Set("gorm:table_options", "ENGINE=InnoDB DEFAULT CHARSET=utf8 comment '用户 数据表'").CreateTable(&model.User{}).Error; err != nil {
			panic(err)
		}
	}

	// 文件 数据表
	if !db.HasTable(&model.User{}) {
		if err := db.Set("gorm:table_options", "ENGINE=InnoDB DEFAULT CHARSET=utf8 comment '文件 数据表'").CreateTable(&model.SmmsFile{}).Error; err != nil {
			panic(err)
		}
	}
}
