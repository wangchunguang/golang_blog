package controller

import (
	"fmt"
	"github.com/dchest/captcha"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"net/http"
	"wangchunguang_blog/system"
	"wangchunguang_blog/utils"
)

// 获取认证方式
func AuthGet(c *gin.Context) {

	authType := c.Param("authType")
	session := sessions.Default(c)
	uuid := utils.UUID()
	session.Delete(SESSION_GITHUB_STATE)
	session.Set(SESSION_GITHUB_STATE, uuid)
	session.Save()
	authrl := "/signin"
	switch authType {
	case "github":
		authrl = fmt.Sprintf(system.GetConfiguration().GithubAuthUrl, system.GetConfiguration().GithubClientId, uuid)
	case "weibo":
	case "qq":
	case "wechat":
	case "oschina":
	default:
	}
	c.Redirect(http.StatusFound, authrl)
}

// 验证码
func CaptchaGet(context *gin.Context) {
	session := sessions.Default(context)
	id := captcha.NewLen(4)
	session.Delete(SESSION_CAPTCHA)
	session.Set(SESSION_CAPTCHA, id)
	session.Save()
	captcha.WriteImage(context.Writer, id, 100, 40)

}
