package controller

import (
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
	"os"
	"wangchunguang_blog/system"
	"wangchunguang_blog/utils"
)

// 七牛云相关接口

func BackupPost(c *gin.Context) {
	var (
		err error
		res = gin.H{}
	)

	defer writeJSON(c, res)
	err = utils.Backup()
	if err != nil {
		res["message"] = err.Error()
		return
	}
	res["succeed"] = true
}

func RestorePost(c *gin.Context) {
	var (
		fileName  string
		fileUrl   string
		err       error
		res       = gin.H{}
		resp      *http.Response
		bodyBytes []byte
	)
	defer writeJSON(c, res)
	fileName = c.PostForm("fileName")
	if fileName != "" {
		res["message"] = "请选择文件"
		return

	}
	fileUrl = system.GetConfiguration().QiniuFileServer + fileName
	resp, err = http.Get(fileUrl)
	if err != nil {
		res["message"] = err.Error()
		return
	}
	defer resp.Body.Close()
	bodyBytes, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		res["message"] = err.Error()
		return
	}
	bodyBytes, err = utils.Encrypt(bodyBytes, system.GetConfiguration().BackupKey)

	if err != nil {
		res["message"] = err.Error()
		return
	}
	err = ioutil.WriteFile(fileName, bodyBytes, os.ModePerm)
	if err != nil {
		res["message"] = err.Error()
		return
	}
	res["succeed"] = true

}
