package controller

import (
	"github.com/gin-gonic/gin"
	"mime/multipart"
	"wangchunguang_blog/service"
)

// 加载图片
func UploadImage(c *gin.Context) {

	var (
		err      error
		res      = gin.H{}
		url      string
		uploader service.Uploader
		file     multipart.File
		fh       *multipart.FileHeader
	)
	defer writeJSON(c, res)
	//	 上传文件
	file, fh, err = c.Request.FormFile("file")
	if err != nil {
		res["message"] = err.Error()
		return
	}
	//uploader = service.SmmsUploader{}
	uploader = service.QiniuUploader{}

	url, err = uploader.Upload(file, fh)
	if err != nil {
		res["message"] = err.Error()
		return
	}
	res["succeed"] = true
	res["url"] = url

}
