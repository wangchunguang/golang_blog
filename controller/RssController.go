package controller

import (
	"fmt"
	"github.com/cihub/seelog"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/feeds"
	"wangchunguang_blog/service"
	"wangchunguang_blog/system"
	"wangchunguang_blog/utils"
)

// 获取订阅
func RssGet(c *gin.Context) {

	time := utils.GetCurrentTime()
	doman := system.GetConfiguration().Domain
	feed := &feeds.Feed{
		Title:       "WCGblog",
		Link:        &feeds.Link{Href: doman},
		Description: "WCGblog,talk about golang,java and so on.",
		Author:      &feeds.Author{Name: "Wangchunguang", Email: "759003662@qq.com"},
		Created:     time,
	}
	feed.Items = make([]*feeds.Item, 0)
	posts, err := service.ListPublishedPost("", 0, 0)
	if err != nil {
		seelog.Error(err)
		return
	}
	for _, post := range posts {
		item := &feeds.Item{
			Id:          fmt.Sprintf("%s/post/%d", doman, post.ID),
			Title:       post.Title,
			Link:        &feeds.Link{Href: fmt.Sprintf("%s/post/%d", doman, post, post.ID)},
			Description: string(service.Excerpt(post)),
			Created:     time,
		}
		feed.Items = append(feed.Items, item)

	}

	rss, err := feed.ToRss()
	if err != nil {
		seelog.Error(err)
		return
	}
	c.Writer.WriteString(rss)

}
