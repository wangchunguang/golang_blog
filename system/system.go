package system

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type Configuration struct {
	DSN                string `yaml:"dsn"`             //数据库地址
	PageSize           int    `yaml:"page_size"`       //尺寸大小
	Addr               string `yaml:"addr"`            //请求端口
	SessionSecret      string `yaml:"session_secret"`  //session
	SignupEnabled      bool   `yaml:"signup_enabled"`  // 是否启动注册
	Public             string `yaml:"public"`          //公开
	Domain             string `yaml:"domain"`          //域名
	BackupKey          string `yaml:"backup_key"`      //备份的key
	QiniuBucket        string `yaml:"qiniu_bucket"`    // 七牛云的名称
	QiniuAccessKey     string `yaml:"qiniu_accesskey"` // 七牛的key
	QiniuSecretKey     string `yaml:"qiniu_secretkey"` // 七牛云sk
	QiniuFileServer    string `yaml:"qiniu_fileserver"`
	GithubClientId     string `yaml:"github_clientid"` // github
	GithubClientSecret string `yaml:"github_clientsecret"`
	GithubAuthUrl      string `yaml:"github_authurl"`
	GithubRedirectURL  string `yaml:"github_redirecturl"`
	GithubTokenUrl     string `yaml:"github_tokenurl"`
	GithubScope        string `yaml:"github_scope"`
	SmmsFileServer     string `yaml:"smms_fileserver"`
	NotifyEmails       string `yaml:"notify_emails"` //通知邮件
	SmtpUsername       string `yaml:"smtp_username"` // 账户
	SmtpPassword       string `yaml:"smtp_password"` //密码
	SmtpHost           string `yaml:"smtp_host"`     //地址
}

const (
	DEFAULT_PAGESIZE = 10
)

var configuration *Configuration

func LoadConfiguration(path string) error {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}
	// 获取文件中的所有数据
	var config Configuration
	// 配置文件解析
	err = yaml.Unmarshal(data, &config)
	if err != nil {
		return nil
	}
	if config.PageSize <= 0 {
		config.PageSize = DEFAULT_PAGESIZE
	}
	configuration = &config
	return err
}
func GetConfiguration() *Configuration {
	return configuration
}
