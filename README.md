## 因为已经有自己的java博客了最近正在学习golang开发 所以用golang技术开发自己的博客

#### 技术选型
1. gorm：是针对数据库操作的框架
2. gin：是go语言对于http的调用
3. 文件存储： 采用的七牛云文件存储，需要的话可以去https://www.qiniu.com/ 自行配置conf文件
4. 邮件通知：采用的是qq邮箱通知进入qq邮箱设置/账户里面/POP3/IMAP/SMTP/Exchange/CardDAV/CalDAV服务  设置与开启
5. mod：配置文件


### 项目结构
```
-wcgblog
    |-conf 配置文件目录
    |-controller 控制器目录
        /- RouterContrller 所有接口访问存储位置
    /- service 所有sql编写位置
        /- CreateTable 所有数据表生成方法   
    |-utils 公共方法目录
    |-models 所有的实体
    |-static 静态资源目录
        |-css css文件目录
        |-images 图片目录
        |-js js文件目录
        |-libs js类库
    |-system 系统配置文件加载目录
    |-views 模板文件目录
    |-main.go 程序执行入口
```
### config.yaml文件配置
```
-config.yaml
 1. dsn: 数据库地址root:root@tcp(127.0.0.1:3306)/blog?charset=utf8&parseTime=True&loc=Local
 2. 如果需求上传图片功能请自行申请七牛云存储空间，并修改配置文件填写
     -  地址https://www.qiniu.com/
     -  qiniu_accesskey  七牛云存储空间ak
     -  qiniu_secretkey  七牛云存储空间sk
     -  qiniu_fileserver 七牛访问地址
     -  qiniu_bucket 空间名称
 3. 如果需要github配置登录信息
     -  访问地址：https://github.com/settings/developers
     -  github_clientid 
     -  github_clientsecret
     -  github_redirecturl
 4. 如果需要使用邮件订阅功能，请自行填写
     -  登录qq邮箱之后修改POP3/IMAP/SMTP服务，让服务开启
     -  smtp_username   smtp账号
     -  smtp_password   smtp密码
     -  smtp_host: smtp.qq.com:465  
 5. Goland运行时，修改main.go getCurrentDirectory方法返回""   
```
### 程序启动方式
#### 采用的mod方式
1. go mod init 项目名称
2. set GO111MODULE=on
3. set GOPROXY=https://goproxy.cn
4. go mod tidy

#### 项目部署
部署到服务器
1. set GOARCH=amd64
2. set GOOS=linux
3. go build main.go
注意： 将所有的配置文件以及所有的静态文件上传到对应的目录下，go语言会打包成一个没有后缀名称的main文件
4. chmod 777 main
5. 控制台启动./main
6. 后台启动nohup ./main &

