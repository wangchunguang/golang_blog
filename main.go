package main

import (
	"flag"
	"github.com/cihub/seelog"
	"wangchunguang_blog/controller"
	"wangchunguang_blog/service"
	"wangchunguang_blog/system"
)

func main() {
	confifFilePath := flag.String("C", "conf/conf.yaml", "config file path")
	logConfigPath := flag.String("L", "conf/seelog.xml", "log config file path")
	flag.Parse()
	logger, err := seelog.LoggerFromConfigAsFile(*logConfigPath)
	if err != nil {
		seelog.Critical("err parsing seelog config file", err)
		return
	}
	seelog.ReplaceLogger(logger)
	defer seelog.Flush()
	if err := system.LoadConfiguration(*confifFilePath); err != nil {
		seelog.Critical("err parsing config log file", err)
		return
	}
	// 连接数据库
	service.ConnectDB()
	defer service.DisconnectDB()
	// 获取接口地址
	router := controller.MapRoutes()
	// 请求端口
	router.Run(system.GetConfiguration().Addr)

}
